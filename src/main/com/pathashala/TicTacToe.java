package com.pathashala;

/*
A Checker for TicTacToe game.
 */
public class TicTacToe {
    private char [][] matrix;

    TicTacToe (char[][] mat) {
        matrix = mat;
    }

    private boolean checkRows(char player) {
        for (int i = 0; i < 3; i++) {
            boolean victory = true;
            for (int j = 0; j < 3; j++) {
                if (matrix[i][j] != player) {
                    victory = false;
                    break;
                }
            }
            if (victory) {
                return true;
            }
        }
        return false;
    }

    private boolean checkColumns(char player) {
        for (int i = 0; i < 3; i++) {
            boolean victory = true;
            for (int j = 0; j < 3; j++) {
                if (matrix[j][i] != player) {
                    victory = false;
                    break;
                }
            }
            if (victory) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonals(char player) {
        return checkLeftToRightDiagonal(player) || checkRightToLeftDiagonal(player);
    }

    private boolean checkRightToLeftDiagonal(char player) {
        boolean victory;
        victory = true;
        for (int i = 0; i < 3; i++) {
            if (matrix[i][2 - i] != player) {
                victory = false;
                break;
            }
        }
        return victory;
    }

    private boolean checkLeftToRightDiagonal(char player) {
        boolean victory = true;
        for (int i = 0; i < 3; i++) {
            if (matrix[i][i] != player) {
                victory = false;
                break;
            }
        }
        return victory;
    }

    public boolean checkDraw() {
        boolean draw = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!isSquareEitherX_or_O(matrix[i][j])) {
                    draw = false;
                }
            }
        }
        return draw;
    }

    private boolean isSquareEitherX_or_O(char c) {
        return c == 'x' || c == 'o';
    }

    public char checkVictory() {
        char players[] = new char[] { 'x', 'o' };
        char winner = ' ';
        for (char player: players) {
            boolean hasWon = hasPlayerWon(player);
            if (hasWon && winner != ' ') {
                return 'a';
            } else if (hasWon) {
                winner = player;
            }
        }
        if (winner != ' ') {
            return winner;
        }
        return '-';
    }

    private boolean hasPlayerWon(char player) {
        return checkColumns(player) || checkRows(player) || checkDiagonals(player);
    }

    public boolean isValid() {
        int xCount = 0, oCount = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrix[i][j] == 'x') {
                    xCount++;
                } else if (matrix[i][j] == 'o') {
                    oCount++;
                }
            }
        }
        return computeValidityOfBoard(xCount, oCount);
    }

    private boolean computeValidityOfBoard(int xCount, int oCount) {
        return (xCount == oCount && checkVictory() == 'o') && (xCount - oCount <= 1) && (xCount - oCount >= 0);
    }

    private void printMatrix() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        new TicTacToe(
                new char[][] {
                        { 'o', 'o', 'x' },
                        { 'o', 'x', 'x' },
                        { 'o', 'x', 'o' }
                }).printMatrix();
    }
}
