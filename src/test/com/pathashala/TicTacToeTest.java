package com.pathashala;

import org.junit.jupiter.api.Test;

class TicTacToeTest {
    @Test
    void TestCase1() {
        assert (new TicTacToe(
                new char[][]{
                        {'o', 'o', 'x'},
                        {'o', 'x', 'x'},
                        {'o', 'x', 'o'}
                }).checkVictory() == 'o');
    }

    @Test
    void TestCase2() {
        assert (new TicTacToe(
                new char[][]{
                        {'o', 'x', 'x'},
                        {'o', 'x', 'o'},
                        {'x', 'o', 'x'}
                }).checkVictory() == 'x');
    }

    @Test
    void TestCase3() {
        assert (new TicTacToe(
                new char[][]{
                        {'o', 'x', 'o'},
                        {'x', 'x', 'o'},
                        {'x', 'o', 'x'}
                }).checkDraw());
    }

    @Test
    void TestCase4() {
        assert (new TicTacToe(
                new char[][]{
                        {'o', 'x', 'o'},
                        {'x', 'x', 'x'},
                        {'o', 'o', 'x'}
                }).checkVictory() == 'x');
    }

    @Test
    void TestCase5() {
        assert (!new TicTacToe(
                new char[][]{
                        {'x', '-', 'o'},
                        {'x', '-', 'o'},
                        {'x', '-', 'o'}
                }).isValid());
    }
}